//
//  io_file.h
//  file_queue
//
//  Created by Леонид Степанчук on 30.09.2020.
//

#ifndef io_file_h
#define io_file_h

#include <stdio.h>

#define WR  "a+t"
#define R   "r+t"

typedef enum{
    ERROR = 0,
    SUCCESS = !ERROR
}ErrorStatus;

ErrorStatus io_open_file(char* path, char* mode);
ErrorStatus io_close_file(void);
ErrorStatus io_write_file(char *str, long size);
ErrorStatus io_read_file(char *str, long size);
long io_get_size_file(void);
ErrorStatus io_seek_file(long seek);
ErrorStatus io_truncate_file(long size);

#endif /* io_file_h */
