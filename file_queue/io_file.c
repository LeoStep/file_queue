//
//  io_file.c
//  file_queue
//
//  Created by Леонид Степанчук on 30.09.2020.
//

#include <string.h>
#include <unistd.h>
#include "io_file.h"
#include "file_queue.h"

FILE *filequeue = NULL;

/*
 *
 */
ErrorStatus io_open_file(char* path, char* mode){
    ErrorStatus status = ERROR;

    filequeue = fopen(path, mode);
    if(filequeue != NULL){
        status = SUCCESS;
    }
    
    return status;
}

/*
 *
 */
ErrorStatus io_close_file(void){
    if(filequeue != NULL){
        int sta = fclose(filequeue);
        if(sta == 0){
            return SUCCESS;
        }
    }
    filequeue = NULL;
    
    return ERROR;
}

/*
 *
 */
ErrorStatus io_write_file(char *str, long size){
    size_t sta = 0;
    
    if((filequeue != NULL) && (str != NULL)){
        sta = fwrite(str, size, 1, filequeue);
    }
    
    if(sta > 0){
        return SUCCESS;
    }
    
    return ERROR;
}

/*
 *
 */
ErrorStatus io_read_file(char *str, long size){
    long sta = 0;
    
    if(filequeue != NULL){
        sta = fread(str, 1, size, filequeue);
    }
    
    if(sta > 0){
        return SUCCESS;
    }
    
    return ERROR;
}

/*
 *
 */
long io_get_size_file(void){
    if(filequeue != NULL){
        fseek (filequeue, 0, SEEK_END);
        return ftell (filequeue);
    }else{
        return 0;
    }
}

/*
 *
 */
ErrorStatus io_seek_file(long seek){
    if(fseek (filequeue, seek ,SEEK_SET) == 0){
        return SUCCESS;
    }
    
    return ERROR;
}

/*
 *
 */
ErrorStatus io_truncate_file(long size){
    long sta = -1;
    
    if(filequeue != NULL){
        int handle = fileno(filequeue);
        sta = ftruncate(handle, size);
    }
    
    if(sta == 0){
        return SUCCESS;
    }
    
    return ERROR;
}
