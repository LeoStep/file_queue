//
//  file_queue.h
//  file_queue
//
//  Created by Леонид Степанчук on 27.09.2020.
//

#ifndef file_queue_h
#define file_queue_h

#include <stdio.h>
#include "io_file.h"

#define CHAR_END    '\n'
#define EXIT_ERROR  if(io_close_file() == ERROR){return ERROR;}return ERROR;

/*
 *
 */
ErrorStatus reade_file_queue(char* str);
ErrorStatus write_file_queue(char* str);
ErrorStatus delete_file_queue(void);

#endif /* file_queue_h */
