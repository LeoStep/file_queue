//
//  file_queue.c
//  file_queue
//
//  Created by Леонид Степанчук on 27.09.2020.
//

#include "file_queue.h"
#include "io_file.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

char path[] = {"/Users/leo/Downloads/queue.txt"};

ErrorStatus write_string_to_file_end(char* str);
ErrorStatus delete_first_string_to_file(void);
ErrorStatus reade_first_string_to_file(char* str);

/**
 * reade queue first element
 * return  - pointer to queue first element
 * NULL reader error
 */
ErrorStatus reade_file_queue(char* str){
    return reade_first_string_to_file(str);
}

/**
 * write to queue end element
 * *str - pointer to string queue element
 * return - status error
 */
ErrorStatus write_file_queue(char* str){
    if(strchr(str, CHAR_END) == 0){
        str[strlen(str)] = CHAR_END;
        str[(strlen(str)+1)] = 0;
    }
    
    return write_string_to_file_end(str);
}

/**
 * delete queue first element
 * return - status error
 */
ErrorStatus delete_file_queue(void){
    return delete_first_string_to_file();
}

/**
 * *str - pointer to string for write
 * return - status error
 */
ErrorStatus write_string_to_file_end(char* str){
    if(io_open_file(path, WR) == ERROR){
        return ERROR;
    }
    
    long sizefile = io_get_size_file();
        
    if(io_seek_file(sizefile) == ERROR){
        EXIT_ERROR;
    }
    
    if(io_write_file(str, strlen(str)) == ERROR){
        EXIT_ERROR;
    }
    
    if(io_close_file() == ERROR){
        return ERROR;
    }

    return SUCCESS;
}

/**
 * return - status error
 */
ErrorStatus delete_first_string_to_file(void){
    if(io_open_file(path, R) == ERROR){
        return ERROR;
    }
    
    long sizefile = io_get_size_file();
    if(sizefile == 0){
        EXIT_ERROR;
    }
    
    char * buffer;
    buffer = (char*) malloc (sizeof(char)*sizefile);
    if(buffer == NULL){
        EXIT_ERROR;
    }
    
    if(io_close_file() == ERROR){
        free (buffer);
        return ERROR;
    }
    
    if(io_open_file(path, WR) == ERROR){
        free (buffer);
        EXIT_ERROR;
    }

    if(io_seek_file(0) == ERROR){
        free (buffer);
        EXIT_ERROR;
    }
                        
    if(io_read_file(buffer, sizefile) == ERROR){
        free (buffer);
        EXIT_ERROR;
    }
    
    char* nchr = strchr(buffer, CHAR_END);
    if(nchr == NULL){
        free (buffer);
        EXIT_ERROR;
    }
                                
    if(io_truncate_file(0) == ERROR){
        free (buffer);
        EXIT_ERROR;
    }
    
    long checksize = (sizefile-((nchr-buffer)+1));
    if(checksize == 0){
        free (buffer);
        if(io_close_file() == ERROR){
            return ERROR;
        }
        return SUCCESS;
    }

    if(io_seek_file(checksize) == ERROR){
        free (buffer);
        EXIT_ERROR;
    }

    if(io_seek_file(0) == ERROR){
        free (buffer);
        EXIT_ERROR;
    }

    if(io_write_file((nchr+1), checksize) == ERROR){
        free (buffer);
        EXIT_ERROR;
    }
                                
    if(io_close_file() == ERROR){
        return ERROR;
    }

    return SUCCESS;
}

/**
 * *str - pointer to string for read
 * return - status error
 */
ErrorStatus reade_first_string_to_file(char* str){
    if(io_open_file(path, R) == ERROR){
        return ERROR;
    }
    
    long sizefile = io_get_size_file();
    if(sizefile ==  0){
        EXIT_ERROR;
    }
    
    char * buffer;
    buffer = (char*) malloc (sizeof(char)*sizefile);
    if(buffer == NULL){
        EXIT_ERROR;
    }
    
    if(io_seek_file(0) == ERROR){
        free (buffer);
        EXIT_ERROR;
    }
    
    if(io_read_file(buffer, sizefile) == ERROR){
        free (buffer);
        EXIT_ERROR;
    }
    
    char* nchr = strchr(buffer, CHAR_END);
    if(nchr == NULL){
        free (buffer);
        EXIT_ERROR;
    }
    
    strncpy(str, buffer, (nchr-buffer));
    free (buffer);

    if(io_close_file() == ERROR){
        return ERROR;
    }
    
    return SUCCESS;
}
